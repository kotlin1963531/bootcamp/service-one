CREATE TABLE files (
    id UUID PRIMARY KEY,
    original_name VARCHAR(255) NOT NULL,
    status VARCHAR(255) NOT NULL
);
