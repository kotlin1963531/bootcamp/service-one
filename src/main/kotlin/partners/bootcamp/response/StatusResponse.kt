package partners.bootcamp.response

import partners.bootcamp.model.enumerable.FileStatus
import io.micronaut.serde.annotation.Serdeable

@Serdeable
data class StatusResponse(
    val status: FileStatus
)
