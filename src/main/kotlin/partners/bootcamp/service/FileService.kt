package partners.bootcamp.service

import partners.bootcamp.integration.event.FileUploadedEvent
import partners.bootcamp.integration.event.producer.FileUploadedProducer
import partners.bootcamp.model.File
import partners.bootcamp.model.enumerable.FileStatus
import partners.bootcamp.repository.FileRepository
import partners.bootcamp.storage.MockFileStorage
import io.micronaut.http.multipart.CompletedFileUpload
import jakarta.inject.Singleton
import java.util.*

@Singleton
class FileService (
    private val fileStorage: MockFileStorage,
    private val fileRepository: FileRepository,
    private val fileUploadedProducer: FileUploadedProducer
) {

    suspend fun uploadFile(fileToUpload: CompletedFileUpload): File {
        val uuid = UUID.randomUUID()
        val file = File(uuid, fileToUpload.filename)

        fileRepository.save(file)
        fileStorage.uploadFile("$uuid.${fileToUpload.contentType.get().extension}", fileToUpload.inputStream)

        fileUploadedProducer.send(FileUploadedEvent(uuid))

        return file
    }

    suspend fun updateFileStatus(uuid: UUID, status: FileStatus) {
        val file = getFile(uuid)
        val updatedFile = file.apply {
            this.status = status
        }

        fileRepository.update(updatedFile)
    }

    suspend fun getFile(uuid: UUID): File {
        return fileRepository.findById(uuid) ?: throw Exception("File not found")
    }
}
