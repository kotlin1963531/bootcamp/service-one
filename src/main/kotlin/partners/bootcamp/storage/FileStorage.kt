package partners.bootcamp.storage

import java.io.InputStream

interface FileStorage {
    suspend fun uploadFile(fileName: String, inputStream: InputStream)
}
