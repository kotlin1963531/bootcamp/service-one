package partners.bootcamp.storage

import jakarta.inject.Singleton
import org.slf4j.LoggerFactory
import java.io.InputStream

@Singleton
class MockFileStorage : FileStorage {

    private val logger = LoggerFactory.getLogger(this::class.java)

    override suspend fun uploadFile(fileName: String, inputStream: InputStream) {
        this.logger.info("MockFileStorage.uploadFile - File: $fileName was successfully saved")
    }
}
