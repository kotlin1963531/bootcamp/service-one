package partners.bootcamp.model.enumerable

enum class FileStatus {
    PROCESSING, NOT_INFECTED, INFECTED, ERROR
}
