package partners.bootcamp.integration.event

import io.micronaut.core.annotation.Introspected
import io.micronaut.serde.annotation.Serdeable
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotEmpty
import jakarta.validation.constraints.NotNull
import java.util.UUID

@Serdeable
@Introspected
data class FileStatusChangedEvent (
    @field:NotNull
    @field:NotBlank
    @field:NotEmpty
    val id: UUID,
    @field:NotNull
    @field:NotBlank
    @field:NotEmpty
    val status: String
)
