package partners.bootcamp.integration.event.listener

import partners.bootcamp.integration.event.FileStatusChangedEvent
import partners.bootcamp.integration.event.initializer.FILE_STATUS_CHANGED_QUEUE_NAME
import partners.bootcamp.model.enumerable.FileStatus
import partners.bootcamp.service.FileService
import io.micronaut.rabbitmq.annotation.Queue
import io.micronaut.rabbitmq.annotation.RabbitListener
import io.micronaut.rabbitmq.bind.RabbitAcknowledgement
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory

@RabbitListener
class FileStatusListener(private val fileService: FileService) {

    private val logger = LoggerFactory.getLogger(this::class.java)

    private val scope: CoroutineScope = CoroutineScope(Dispatchers.IO)

    @Queue(FILE_STATUS_CHANGED_QUEUE_NAME)
    fun handleFileStatusChangeEvent(event: FileStatusChangedEvent, acknowledgement: RabbitAcknowledgement) {
        scope.launch {
            logger.info("FileStatusListener.handleFileStatusChangeEvent - File ${event.id} comes with new status ${event.status}")
            try {
                fileService.updateFileStatus(
                    event.id,
                    FileStatus.valueOf(event.status)
                )
                acknowledgement.ack()
            } catch (e: Exception) {
                logger.error("FileStatusListener: Error processing message", e)
                fileService.updateFileStatus(
                    event.id,
                    FileStatus.ERROR
                )
                acknowledgement.nack(false, false)
            }
        }
    }
}

