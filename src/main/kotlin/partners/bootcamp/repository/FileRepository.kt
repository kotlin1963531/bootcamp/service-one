package partners.bootcamp.repository

import partners.bootcamp.model.File
import io.micronaut.data.annotation.Repository
import io.micronaut.data.repository.kotlin.CoroutineCrudRepository
import java.util.UUID

@Repository
interface FileRepository : CoroutineCrudRepository<File, UUID>
